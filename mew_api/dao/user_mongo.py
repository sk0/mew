# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/7 下午7:01
"""
from mew_api.dao.template.MongoTemplate import MongodbTemplate


class User(MongodbTemplate):
    def __init__(self):
        super(User, self).__init__(collection='sms')


if __name__ == '__main__':
    print(list(User().find_many().limit(1)))
