# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/7 下午7:21
"""
from mew_api.app import app

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)
