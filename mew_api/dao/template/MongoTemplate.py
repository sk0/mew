# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/7 下午4:27
"""
from mew_api.dao import biz_db


class MongodbTemplate(object):
    def __init__(self, db=biz_db, collection=None):
        self.db = db
        self.collection = getattr(self.db, collection)

    def find_many(self, *args, **kwargs):
        return self.collection.find(*args, **kwargs)

    def find_one(self, *args, **kwargs):
        return self.collection.find_one(*args, **kwargs)

    def update(self, *args, **kwargs):
        self.collection.update(*args, **kwargs)

    def insert(self, bean: dict):
        self.collection.insert(bean)


if __name__ == '__main__':
    print(list(MongodbTemplate('sms').find_many({}).limit(1)))
