# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/7 下午6:22
"""
import pymongo
import pymysql


def get_mongodb_by_config(cf):
    """
    :param cf:
    :return:
    """
    uri = 'mongodb://{username}:{password}@{host1}:{port1}/{database}'.format(
        username=cf.get('username'),
        password=cf.get('password'),
        host1=cf.get('host1'),
        port1=cf.get('port1'),
        database=cf.get('database')
    )

    kwargs = {
        'minPoolSize': cf.get('minPoolSize'),
        'maxPoolSize': cf.get('maxPoolSize'),
        # 'waitQueueMultiple': cf.get('waitQueueMultiple'),
        # 'waitQueueTimeoutMS': cf.get('waitQueueTimeoutMS'),
        # 'connectTimeoutMS': 1,
        # 'socketTimeoutMS': 1,
        'serverSelectionTimeoutMS': 5000,  # 建立连接超时时间5s
    }

    return pymongo.MongoClient(uri, **kwargs)[cf.get('database')]


def get_connection(conf):
    # Connect to the database
    return pymysql.connect(
        host=conf['host'],
        user=conf['user'],
        password=conf['password'],
        db=conf['db'],
        charset=conf['charset'],
        cursorclass=pymysql.cursors.DictCursor
    )
