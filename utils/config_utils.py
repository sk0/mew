# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/2 上午10:02
"""
import yaml
import os

path = '{}/conf/{}'.format(os.path.dirname(os.path.dirname(__file__)), 'application.yml')


def load_config():
    """
    加载配置文件
    :return:
    """
    # 环境判断 取不同配置文件
    with open(path) as file:
        dc_config = yaml.load(file)
    return dc_config


config = load_config()
