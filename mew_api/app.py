# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/7 下午7:21
"""
import logging

from flask import Flask, jsonify

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S %p"

logging.basicConfig(filename='mew.log', level=logging.DEBUG, format=LOG_FORMAT, datefmt=DATE_FORMAT)

app = Flask(__name__)


@app.route('/health')
def hello_world():
    return jsonify({'status': "UP"})
