# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/7 下午6:08
"""
from mew_api.dao import mysql_biz_conf
from mew_api.dao.template import get_connection


class MySqlTemplate(object):
    @staticmethod
    def sqlManyExec(sql: str):
        connection = get_connection(conf=mysql_biz_conf)
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql)
                return cursor.fetchall()
        finally:
            connection.close()

    @staticmethod
    def sqlOneExec(sql: str):
        connection = get_connection(conf=mysql_biz_conf)
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql)
                return cursor.fetchone()
        finally:
            connection.close()

    @staticmethod
    def sqlInsert(sql: str):
        connection = get_connection(conf=mysql_biz_conf)
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql)
            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
        finally:
            connection.close()
