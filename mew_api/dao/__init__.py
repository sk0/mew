# -*- coding: utf-8 -*-
"""
@contact: lishulong.never@gmail.com
@time: 2018/8/7 下午4:19
"""
from mew_api.dao.template import get_mongodb_by_config
from utils import config

# mongo init
biz_db = get_mongodb_by_config(config.get('mongo').get('biz'))
# mysql init
mysql_biz_conf = config.get('mysql').get('biz')

if __name__ == '__main__':
    pass
